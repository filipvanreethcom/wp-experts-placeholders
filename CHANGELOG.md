# Changelog 
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
* LICENSE.txt file

## [0.0.5] - 2018-02-17
### Added
* 16:9 SVG Image Placeholder
* A CHANGELOG.md

## [Previous versions] - Prior to version [0.0.5]
- Learned to setup a Bower package and GIT versioning by ups and down.
